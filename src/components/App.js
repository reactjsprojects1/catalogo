import React, { useEffect, useState } from 'react';
import MovieCard from './MovieCard';
import './App.css';
import SearchIcon from './search.svg';
// 979c7004
const API = 'http://www.omdbapi.com?apikey=979c7004';


function App(){
  const [movies, setMovies] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  
  const searchMovies = async (title) => {
    const url = `${API}&s=${title}`;
    const response = await fetch(url);
    const data = await response.json();
    setMovies(data.Search);
    console.log(data.Search);
  };
  
  useEffect(() => {
    searchMovies('los viajes del viento');
  },[]);
  
  return(
    <div className='app'>
      <h1>Pelisteka</h1>
      <div className='search'>
        <input
          placeholder='busca una peli'
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <img
          src={SearchIcon}
          alt="search"
          onClick={() => searchMovies(searchTerm) }
        />
      </div>

      {
        movies.length > 0 ?
          (
            <div className='container'>
              {movies.map((movie) => (<MovieCard movie={movie} key={movie.imdbID}/>))}

            </div>
          ) : (
            <div className='empty'>
              <h2>No se encontraron pelis</h2>
            </div>
          )

      }     
    </div>
  );
}

export default App;
